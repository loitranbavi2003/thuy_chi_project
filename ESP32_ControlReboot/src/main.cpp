#include <Arduino.h>

#define PIN_BTN1 18   // MDI_RP_P1
#define PIN_BTN2 19   // MDI_TN_P1

static long long count_time = 0;

void setup() {
  Serial.begin(115200);
  pinMode(PIN_BTN1, OUTPUT);
  pinMode(PIN_BTN2, INPUT);

  digitalWrite(PIN_BTN1, HIGH);
}

void loop() 
{
  digitalWrite(PIN_BTN1, HIGH);

  if(digitalRead(PIN_BTN2) == 1)
  {
    count_time = 0;
  }
  else
  {
    count_time++;
  }

  if(count_time >= 180000)
  {
    count_time = 0;
    digitalWrite(PIN_BTN1, LOW);
  }

  delay(1);
}
