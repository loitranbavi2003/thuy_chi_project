#include <iostream>
#include <fstream>
#include <unistd.h>

using namespace std;

#define GPIO_BTN2_LOW             0
#define GPIO_BTN2_HIGH            1
#define BTN2                      "/sys/class/leds/BTN2/brightness"

std::ofstream BTN2_brightness;

int BTN2_Enable(void);
void BTN2_SetUp(void);
void BTN2_SetDown(void);
void BTN2_DisEnable(void);

int main()
{
    BTN2_Enable();

    while(1)
    {
        cout << "valid: 1" << endl;
        BTN2_SetUp();
        sleep(1);

        cout << "valid: 0" << endl;
        BTN2_SetDown();
        sleep(1);
    }

    BTN2_DisEnable();

    return 0;
}

int BTN2_Enable(void)
{
    BTN2_brightness.open(BTN2, std::ios::trunc);

    if (!BTN2_brightness.is_open()) 
    {
        std::cout << "Failed to open the file." << std::endl;
        return 0;
    }
    return 1;
}

void BTN2_SetUp(void)
{
    BTN2_brightness << GPIO_BTN2_HIGH;  
    BTN2_brightness.flush();  
}

void BTN2_SetDown(void)
{
    BTN2_brightness << GPIO_BTN2_LOW;
    BTN2_brightness.flush();
}

void BTN2_DisEnable(void)
{
    BTN2_brightness << GPIO_BTN2_LOW;
    BTN2_brightness.flush();
    BTN2_brightness.close();  
}

