#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

#define GPIO_RS485_LOW 0
#define GPIO_RS485_HIGH 1
#define MAX485 "/sys/class/leds/485_REDE/brightness"

int MAX485_Enable(void);
void MAX485_SetUp(void);
void MAX485_SetDown(void);
void MAX485_DisEnable(void);

int MAX485_fd;

int main()
{
    MAX485_Enable();

    while (1)
    {
        printf("valid: 1\n");
        MAX485_SetUp();
        sleep(1);

        printf("valid: 0\n");
        MAX485_SetDown();
        sleep(1);
    }

    MAX485_DisEnable();

    return 0;
}

int MAX485_Enable(void)
{
    MAX485_fd = open(MAX485, O_WRONLY | O_TRUNC);

    if (MAX485_fd == -1)
    {
        printf("Failed to open the file.\n");
        return 0;
    }

    return 1;
}

void MAX485_SetUp(void)
{
    dprintf(MAX485_fd, "%d", GPIO_RS485_HIGH);
    fsync(MAX485_fd);
}

void MAX485_SetDown(void)
{
    dprintf(MAX485_fd, "%d", GPIO_RS485_LOW);
    fsync(MAX485_fd);
}

void MAX485_DisEnable(void)
{
    dprintf(MAX485_fd, "%d", GPIO_RS485_LOW);
    fsync(MAX485_fd);
    close(MAX485_fd);
}
