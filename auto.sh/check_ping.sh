while true; do
    ping -c 3 8.8.8.8

    if [ $? -ne 0 ]; then
        /etc/init.d/network restart
        echo "Network service restarted."
    else
        echo "Ping to 8.8.8.8 successful."
    fi

    sleep 300
done
