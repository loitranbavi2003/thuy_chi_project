#include "tc_format.h"

/* A REGISTER IS 2 BYTES */

/**
 * @brief Led blink
 *
 * @param data
 * @param led
 * @return int
 */
int Formatter::Data2Register(uint16_t *data, Led &led)
{
    data[0] = led.led;
    data[1] = led.on_time;
    data[2] = led.off_time;

    return 3;
}

/**
 * @brief Ping Response
 *
 * @param data
 * @param value
 * @return int
 */
int Formatter::Data2Register(uint16_t *data, Value &value)
{
    data_format_uint8_16 tmp;
    int count = 0;
    int length = sizeof(value.phone_num) / sizeof(value.phone_num[0]);
    for (uint8_t i = 0; i < length; i++)
    {
        tmp.data_uint8[1] = value.phone_num[i];
        tmp.data_uint8[0] = value.phone_num[++i];
        data[count++] = tmp.data_uint16;
    }
    for (uint8_t i = 0; i < value.data_len; i++)
    {
        tmp.data_uint8[1] = value.data[i];
        tmp.data_uint8[0] = value.data[++i];
        data[count++] = tmp.data_uint16;
    }

    return count;
}

/**
 * @brief Data for Config update frequency
 *
 * @param configUp - 2 registers
 * @return length - length of data
 */
int Formatter::Data2Register(uint16_t *data, uint32_t update_freq)
{
    data_format_uint16_32 tmp16_32;
    tmp16_32.data_uint32 = update_freq;
    data[0] = tmp16_32.bytes[1];
    data[1] = tmp16_32.bytes[0];

    return 2;
}

/**
 * @brief State Response
 *
 * @param data
 * @param config
 * @return int
 */
int Formatter::StateResponse(uint16_t *data, Config &config)
{
    data_format_uint8_16 tmp816;
    data_format_float_uint16 tmpf16;
    data_format_uint16_32 tmp1632;
    data_format_uint16_64 tmp1664;

    int count = 0;
    int length = sizeof(config.phone_num) / sizeof(config.phone_num[0]);
    for (uint8_t i = 0; i < length; i++)
    {
        tmp816.data_uint8[1] = config.phone_num[i];
        tmp816.data_uint8[0] = config.phone_num[++i];
        data[count++] = tmp816.data_uint16;
    }

    length = sizeof(config.MAC) / sizeof(config.MAC[0]);
    for (uint8_t i = 0; i < length; i++)
    {
        tmp816.data_uint8[1] = config.MAC[i];
        tmp816.data_uint8[0] = config.MAC[++i];
        data[count++] = tmp816.data_uint16;
    }

    length = sizeof(config.location) / sizeof(config.location[0]);
    for (uint8_t i = 0; i < length; i++)
    {
        tmp816.data_uint8[1] = config.location[i];
        tmp816.data_uint8[0] = config.location[++i];
        data[count++] = tmp816.data_uint16;
    }

    tmpf16.data_float = config.sensor;
    data[count++] = tmpf16.bytes[1];
    data[count++] = tmpf16.bytes[0];

    tmp816.data_uint8[1] = config.pin_percent;
    tmp816.data_uint8[0] = config.SIM;
    data[count++] = tmp816.data_uint16;

    tmp1632.data_uint32 = config.internet;
    data[count++] = tmp1632.bytes[1];
    data[count++] = tmp1632.bytes[0];

    tmp1632.data_uint32 = config.mqtt_disc;
    data[count++] = tmp1632.bytes[1];
    data[count++] = tmp1632.bytes[0];

    tmp1632.data_uint32 = config.msg_succ;
    data[count++] = tmp1664.bytes[1];
    data[count++] = tmp1664.bytes[0];

    tmp1632.data_uint32 = config.mqtt_conn;
    data[count++] = tmp1632.bytes[1];
    data[count++] = tmp1632.bytes[0];

    tmp816.data_uint8[1] = config.mqtt_state;
    tmp816.data_uint8[0] = config.reset;
    data[count++] = tmp816.data_uint16;

    return count;
}

/**
 * @brief Position configuration
 *
 * @param data
 * @param position
 * @return int
 */
int Formatter::Data2Register(uint16_t *data, Position &position)
{
    int count = 0;
    data_format_uint8_16 tmp;

    for (int i = 0; i < 4; i++)
    {
        tmp.data_uint8[1] = position.longitude[i];
        tmp.data_uint8[0] = position.longitude[++i];
        data[count++] = tmp.data_uint16;
    }

    for (int i = 0; i < 4; i++)
    {
        tmp.data_uint8[1] = position.latitude[i];
        tmp.data_uint8[0] = position.latitude[++i];
        data[count++] = tmp.data_uint16;
    }

    return count;
}

/**
 * @brief MQTT Configuration
 *
 * @param data
 * @param config
 * @return int
 */
int Formatter::MqttConfig(uint16_t *data, Config &config)
{
    data_format_uint8_16 tmp;
    int count = 0;
    int length = sizeof(config.host) / sizeof(config.host[0]);
    for (int i = 0; i < length; i++)
    {
        data[count++] = ByteToInt16(config.host[i], config.host[++i]);
    }
    
    data[count++] = config.port;

    length = sizeof(config.username) / sizeof(config.username[0]);
    for (int i = 0; i < length; i++)
    {
        data[count++] = ByteToInt16(config.username[i], config.username[++i]);
    }

    length = sizeof(config.password) / sizeof(config.password[0]);
    for (int i = 0; i < length; i++)
    {
        data[count++] = ByteToInt16(config.password[i], config.password[++i]);
    }

    length = sizeof(config.main_sub_topic) / sizeof(config.main_sub_topic[0]);
    for (int i = 0; i < length; i++)
    {
        data[count++] = ByteToInt16(config.main_sub_topic[i], config.main_sub_topic[++i]);
    }

    length = sizeof(config.main_pub_topic) / sizeof(config.main_pub_topic[0]);
    for (int i = 0; i < length; i++)
    {
        data[count++] = ByteToInt16(config.main_pub_topic[i], config.main_pub_topic[++i]);
    }

    return count;
}