#include "tc_debug.h"

time_t now;

void DebugLog::Log_Init()
{
#if ENA_LIB_LOG
#if DEBUG_CONSOLE == 1 && DEBUG_FILE == 0
	static plog::ColorConsoleAppender<plog::TxtFormatter> consoleAppender;
	plog::init(plog::verbose, &consoleAppender);
#elif DEBUG_CONSOLE == 0 && DEBUG_FILE == 1
	// static plog::RollingFileAppender<plog::TxtFormatter> fileAppender("logg.txt", 1024, 3);
	// plog::init(plog::error, &fileAppender);
	plog::init(plog::verbose, "Modbus.txt");
#elif DEBUG_CONSOLE == 1 && DEBUG_FILE == 1
	static plog::ColorConsoleAppender<plog::TxtFormatter> consoleAppender;
	static plog::RollingFileAppender<plog::TxtFormatter> fileAppender("log.txt", 1024 * 1024, 5);
	plog::init(plog::verbose, &fileAppender).addAppender(&consoleAppender);
#endif

	PLOG_VERBOSE << "Log Start!"; // short macro
#else
	std::cout << "Log Console" << std::endl;
#endif
}

/**
 * @brief Logs an error message to the debug log.
 *
 * This function logs an error message to the debug log, along with a timestamp indicating when
 * the error occurred. The error message is passed as a parameter to the function.
 *
 * @param str The error message to be logged.
 */
void DebugLog::DebugError(const char *err)
{
#if ENA_LIB_LOG
	PLOG_ERROR << err;
#else
	std::cerr << err << std::endl;
#endif
}

void DebugLog::DebugError(std::string err)
{
#if ENA_LIB_LOG
	PLOG_ERROR << err;
#else
	std::cerr << err << std::endl;
#endif
}

/**
 * @brief Logs a status message to the debug log.
 *
 * This function logs a status message to the debug log, along with a timestamp indicating when
 * the message was received. The status message is passed as a parameter to the function.
 *
 * @param str The status message to be logged.
 */
void DebugLog::DebugStatus(std::string stt)
{
#if ENA_LIB_LOG
	PLOG_VERBOSE << stt;
#else
	std::cout << "|||||||||||||(Status: "<< stt << ")|||||||||||||\n";
#endif
}

/**
 * @brief Logs a custom message to the debug log.
 *
 * This function logs a custom message to the debug log, along with a timestamp indicating when
 * the message was received. The message consists of a header integer value and a vector of strings
 * representing the content of the message. The function formats the message into a single string
 * before logging it to the debug log.
 *
 * @param header An integer value representing the type or category of the message.
 * @param content A vector of strings representing the content of the message.
 */
void DebugLog::DebugLOG(int header, std::vector<std::string> content)
{
#if ENA_LIB_LOG
	switch (header)
	{

	default:
		break;
	}
#else
	std::cout << "----------------------------------------------------------------" << std::endl;
	now = time(0);
	std::cout << "Time: " << now << "---";
	switch (header)
	{

	default:
		break;
	}
#endif
}
