#include "main.h"

static DebugLog DebugLogger;
static ModBus TcModbus;
static Formatter format;
static Sensor sensors[SENSOR_NUM];
static Button buttons[BUTTON_NUM];
static Config config;
static Position position;
static Value value;
static Led led;

static void ReadTask(int state_r);
static void WriteTask(int state_w, uint16_t *data);

int main(int argc, char **argv)
{
    DebugLogger.Log_Init();
    if (argc == 3)
    {
        std::cout << argv[1] << std::endl
                  << argv[2] << std::endl;
        TcModbus.SetDevicePort(std::string(argv[1]));
        TcModbus.SetBaudRate(atoi(argv[2]));
        TcModbus.Connect();
    }
    else
    {
        TcModbus.SetDevicePort(std::string(DEVICE_PORT));
        TcModbus.SetBaudRate(BAUDRATE);
        TcModbus.Connect();
    }
    uint16_t data[100];

    while (true)
    {
        if (TcModbus.CheckConnect() != RETURN_ERROR)
        {
            TcModbus.SetSlave(MB_SLAVE_ADDRESS);
            for (int i = FSM_START_R; i <= FSM_END_R; i++)
            {
                ReadTask(i);
            }
            for (int i = FSM_START_W; i <= FSM_END_W; i++)
            {
                memset(data, sizeof(data) / sizeof(data[0]), 0);
                WriteTask(i, data);
                sleep(1);
            }
            memset(data, sizeof(data) / sizeof(data[0]), 0);
            TcModbus.Close();
            usleep(5);
        }
        else
        {
            TcModbus.Free();
            sleep(5);
            TcModbus.Connect();
        }
    }
}

static void ReadTask(int state_r)
{
    switch (state_r)
    {
    case FSM_START_R:
        // PLOG_VERBOSE << "-------------- Reading Start --------------";
        std::cout << "-------------- Reading Start --------------\n";
        break;

    case FSM_SENSOR_R:
        if (TcModbus.ReadSensors(sensors) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read sensor");
        }
        else
        {
            std::cout << "Sensor read" << std::endl;
            for (int i = 0; i < MB_SENSOR_REG_NUM_R / 2; i++)
            {
                std::cout << sensors[i].value << " ";
            }
            std::cout << std::endl;
        }
        break;

    case FSM_BUTTON_R:
        if (TcModbus.ReadButtons(buttons) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read button");
        }
        else
        {
            std::cout << "Button read" << std::endl;
            for (int i = 0; i < MB_BUTTON_REG_NUM_R * 2; i++)
            {
                std::cout << static_cast<int>(buttons[i].state) << " ";
            }
            std::cout << std::endl;
        }
        break;

    case FSM_BUTTON_CHANGE_R:
        uint16_t buttonChange;
        if (TcModbus.ReadButtonStateChange(buttonChange) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read button change");
        }
        else
        {
            std::cout << "State Change: " << buttonChange << std::endl;
        }
        break;

    case FSM_NEW_MSG_R:
        uint16_t newMsg;
        if (TcModbus.ReadPing(newMsg) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read new message");
        }
        else
        {
            std::cout << "New msg: " << newMsg << std::endl;
        }
        break;

    case FSM_PING_STATE_R:
        if (TcModbus.ReadState(1, value) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read ping state");
        }
        else
        {
            std::cout << "Ping State" << std::endl;
            std::cout << "      State: " << value.state << std::endl;
            std::cout << "      Phone: ";
            for (int i = 0; i < 10; i++)
            {
                std::cout << static_cast<int>(value.phone_num[i]);
            }
            std::cout << std::endl;
        }
        break;

    case FSM_PARAMETER_R:
        if (TcModbus.ReadParameters(config) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read parameters");
        }
        else
        {
            std::cout << "Parameters: ";
            std::cout << config.paramA << " " << config.paramB << " " << config.paramC << std::endl;
        }
        break;

    case FSM_STATE_R:
        if (TcModbus.ReadState(0, value) == RETURN_ERROR)
        {
            DebugLogger.DebugError("Failed to read state");
        }
        else
        {
            std::cout << "State read" << std::endl;
            std::cout << "      State: " << value.state << std::endl;
            std::cout << "      Phone: ";
            for (int i = 0; i < 10; i++)
            {
                std::cout << static_cast<int>(value.phone_num[i]);
            }
            std::cout << std::endl;
        }
        break;

    case FSM_END_R:
        // PLOG_VERBOSE << "-------------- Reading Done --------------";
        std::cout << "-------------- Reading Done --------------\n";
        break;
    default:
        break;
    }
}

static void WriteTask(int state_w, uint16_t *data)
{
    int length{0};
    switch (state_w)
    {
    case FSM_START_W:
        // PLOG_VERBOSE << "-------------- Writing Start --------------";
        std::cout << "-------------- Writing Start --------------\n";
        break;

    case FSM_SINGLE_LED_W:
        length = TcModbus.WriteSingleLed(led.led, led.value);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing Single LED failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_LEDS_W:
        format.Data2Register(data, led);
        length = TcModbus.WriteLeds(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing Leds failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_PING_RESP_W:
        format.Data2Register(data, value);
        length = TcModbus.WritePingResp(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing Ping response failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_UPDATE_FREQ_W:
        format.Data2Register(data, config.update_freq);
        length = TcModbus.WriteConfigUpdate(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing config update failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_STATE_RESP_W:
        format.StateResponse(data, config);
        length = TcModbus.WriteStateResp(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing State response failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_POSITION_W:
        format.Data2Register(data, position);
        length = TcModbus.WritePosition(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing Position failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_MQTT_W:
        format.MqttConfig(data, config);
        length = TcModbus.WriteMqtt(data);
        if(length == RETURN_ERROR)
        {
            DebugLogger.DebugError("Writing MQTT failed");
        } else {
            std::cout << length << std::endl;
        }
        break;

    case FSM_END_W:
        // PLOG_VERBOSE << "-------------- Writing Done --------------";
        std::cout << "-------------- Writing Done --------------\n";
        break;

    default:
        break;
    }
}