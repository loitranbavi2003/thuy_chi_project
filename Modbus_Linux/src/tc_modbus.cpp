#include "tc_modbus.h"

ModBus::ModBus()
{
}

/**
   @brief Destroy the Mod Bus:: Mod Bus object

*/
ModBus::~ModBus()
{
    DebugLog DebugLogger;
    DebugLogger.DebugStatus("ModBus::ModBus Free");
    modbus_close(this->ctx);
    modbus_free(this->ctx);
}

/**
   @brief Connects to a Modbus device using a serial port.

   @param devicePort The serial port to use.
   @param baudRate The baud rate to use for the serial communication.
   @return int16_t Returns 0 on success or an error code if the connection fails.
*/
int ModBus::Connect(const char *devicePort, int baudRate)
{
    DebugLog DebugLogger;
    this->ctx = modbus_new_rtu(devicePort, baudRate, 'N', 8, 1);
    if (this->ctx == NULL)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::Connect Unable to create the libmodbus context");
#endif
        return RETURN_ERROR;
    }

#if ENABLE_DEBUG_MODBUS_STATUS
    DebugLogger.DebugStatus("ModBus::Connect Create Done");
#endif
    return 1;
}

int ModBus::Connect(void)
{
    DebugLog DebugLogger;
    this->ctx = modbus_new_rtu((this->devicePort).c_str(), this->baudRate, 'N', 8, 1);
    if (this->ctx == NULL)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::Connect Unable to create the libmodbus context");
        DebugLogger.DebugError(modbus_strerror(errno));
#endif
        return RETURN_ERROR;
    }

#if ENABLE_DEBUG_MODBUS_STATUS
    DebugLogger.DebugStatus("ModBus::Connect Create Done");
#endif
    return 1;
}

/**
   @brief Sets the Modbus slave address that the class instance will communicate with.

   @param slave_addr The Modbus slave address to use.
*/
void ModBus::SetSlave(int slave_addr)
{
    modbus_set_slave(this->ctx, slave_addr);
}

void ModBus::SetDevicePort(std::string devicePort)
{
    this->devicePort = devicePort;
}

void ModBus::SetBaudRate(int baudRate)
{
    this->baudRate = baudRate;
}

std::string ModBus::GetDevicePort()
{
    return this->devicePort;
}

int ModBus::GetBaudRate()
{
    return this->baudRate;
}

/**
   @brief Checks the connection to the Modbus slave.

   @return Returns 0 if the connection is established, and a negative error code if it is not.
*/
int ModBus::CheckConnect(void)
{
    DebugLog DebugLogger;
    if (modbus_connect(this->ctx) == -1)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::CheckConnect Error Connecting");
        DebugLogger.DebugError(modbus_strerror(errno));
#endif
        return RETURN_ERROR;
    }
    return 1;
}

/**
   @brief Close the connection to the Modbus device.
*/
void ModBus::Close(void)
{
    modbus_close(this->ctx);
}

void ModBus::Free(void)
{
    modbus_close(this->ctx);
    modbus_free(this->ctx);
}

/**
   @brief Reads one or more holding registers from a Modbus slave.

   @param addr The starting address of the registers to be read.
   @param nb The number of registers to be read.
   @param dest Pointer to an array where the read values will be stored.
   @return Returns the number of registers read if successful, or a negative error code if the operation failed.
*/
int ModBus::ReadRegister(int addr, int nb, uint16_t *dest)
{
    int length = 0;
    DebugLog DebugLogger;
    length = modbus_read_registers(ctx, addr, nb, dest);

    if (length == -1)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::ReadRegister");
        DebugLogger.DebugError(modbus_strerror(errno));
#endif
        return RETURN_ERROR;
    }
    return length;
}

/**
 * @brief
 *
 * @param reg_addr
 * @param value
 * @return int
 */
int ModBus::WriteSingleRegister(int reg_addr, uint16_t value)
{
    int length;
    DebugLog DebugLogger;
    length = modbus_write_register(ctx, reg_addr, value);
    if (length == -1)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::WriteSingleRegister");
        DebugLogger.DebugError(modbus_strerror(errno));
#endif
        return RETURN_ERROR;
    }
    return length;
}

/**
 * @brief
 *
 * @param addr
 * @param nb
 * @param data
 * @return int
 */
int ModBus::WriteMultipleRegisters(int addr, int nb, uint16_t *data)
{
    int length;
    DebugLog DebugLogger;
    length = modbus_write_registers(ctx, addr, nb, data);
    if (length == -1)
    {
#if ENABLE_DEBUG_MODBUS_ERROR
        DebugLogger.DebugError("Error ModBus::WriteMultipleRegisters");
        DebugLogger.DebugError(modbus_strerror(errno));
#endif
        return RETURN_ERROR;
    }
    return length;
}

int ModBus::ReadSensors(Sensor *sensors)
{
    int length{0}, count{0};
    uint16_t tab_reg[MB_SENSOR_REG_NUM_R];
    data_format_float_uint16 data;

    length = this->ReadRegister(MB_SENSOR_REG_START_R,
                                MB_SENSOR_REG_NUM_R, tab_reg);
    if (length > 0)
    {
        for (uint8_t i = 0; i < length; i++)
        {
            data.bytes[0] = tab_reg[i];
            data.bytes[1] = tab_reg[++i];
            sensors[count++].value = data.data_float;
        }
        return count;
    }
    return RETURN_ERROR;
}

int ModBus::ReadButtons(Button *buttons)
{
    int length, count{0};
    uint16_t tab_reg[MB_BUTTON_REG_NUM_R];

    length = this->ReadRegister(MB_BUTTON_REG_START_R,
                                MB_BUTTON_REG_NUM_R, tab_reg);
    if (length > 0)
    {
        for (uint8_t i = 0; i < length; i++)
        {
            buttons[count++].state = (tab_reg[i] & 0x00ff);
            buttons[count++].state = ((tab_reg[i] >> 8) & 0x00ff);
        }
        return length * 2;
    }
    return RETURN_ERROR;
}

int ModBus::ReadButtonStateChange(uint16_t &button)
{
    int length;
    length = this->ReadRegister(MB_BUTTON_STATE_REG_R, 1, &button);

    if (length > 0)
    {
        return length * 2;
    }
    return RETURN_ERROR;
}

int ModBus::ReadPing(uint16_t &ping)
{
    int length;
    length = this->ReadRegister(MB_PING_REG_R, 1, &ping);
    if (length > 0)
    {
        return length * 2;
    }
    return RETURN_ERROR;
}

int ModBus::ReadState(uint8_t ping, Value &value)
{
    int length;
    int count = 0;
    uint16_t tab_reg[MB_STATE_REG_NUM_R];
    data_format_uint8_16 data;

    if (ping == 1)
    {
        length = this->ReadRegister(MB_PING_STATE_REG_START_R,
                                    MB_STATE_REG_NUM_R, tab_reg);
    }
    else
    {
        length = this->ReadRegister(MB_STATE_REG_START_R,
                                    MB_STATE_REG_NUM_R, tab_reg);
    }

    if (length > 0)
    {
        if (tab_reg[0])
        {
            value.state = true;
        }
        else
            value.state = false;
        for (uint8_t i = 1; i < length; i++)
        {
            data.data_uint16 = tab_reg[i];
            value.phone_num[count++] = (data.data_uint8[0] - '0');
            value.phone_num[count++] = (data.data_uint8[1] - '0');
        }
        return length;
    }
    return RETURN_ERROR;
}

int ModBus::ReadParameters(Config &config)
{
    int length;
    uint8_t count = 0;
    uint16_t tab_reg[MB_CONFIG_REG_NUM_R];
    data_format_float_uint16 data;

    length = this->ReadRegister(MB_CONFIG_REG_START_R,
                                MB_CONFIG_REG_NUM_R, tab_reg);
    if (length > 0)
    {
        for (uint8_t i = 0; i < length; i++)
        {
            data.bytes[0] = tab_reg[i];
            data.bytes[1] = tab_reg[++i];
            if (count == 0)
            {
                config.paramA = data.data_float;
                count++;
            }
            else if (count == 1)
            {
                config.paramB = data.data_float;
                count++;
            }
            else if (count == 2)
            {
                config.paramC = data.data_float;
                count = 0;
            }
        }
        return length * 2;
    }
    return RETURN_ERROR;
}

int ModBus::WriteSingleLed(uint8_t led, uint8_t value)
{
    int length;
    length = this->WriteSingleRegister(MB_SINGLE_LED_REG_W, ByteToInt16(led, value));

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}

/**
 * @brief Write holding register to Slave
 *
 * @param data[0]: 0x3d: led number
 * @param data[1]: 0x3e: on time
 * @param data[2]: 0x3f: off time
 * @return int16_t
 */
int ModBus::WriteLeds(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_LED_REG_START_W,
                                          MB_LED_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}

int ModBus::WritePingResp(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_PING_RESP_REG_START_W,
                                          MB_PING_RESP_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}

int ModBus::WriteConfigUpdate(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_CONFIG_FREQ_REG_START_W,
                                          MB_CONFIG_FREQ_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}

int ModBus::WriteStateResp(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_STATE_RESP_REG_START_W,
                                          MB_STATE_RESP_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}

int ModBus::WritePosition(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_POSITION_REG_START_W,
                                          MB_POSITION_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}
int ModBus::WriteMqtt(uint16_t *data)
{
    int length;

    length = this->WriteMultipleRegisters(MB_MQTT_REG_START_W,
                                          MB_MQTT_REG_NUM_W,
                                          data);

    if (length > 0)
    {
        return length;
    }
    return RETURN_ERROR;
}