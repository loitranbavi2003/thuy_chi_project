#ifndef MAIN_H
#define MAIN_H

#include <ctime>
#include <vector>
#include <iostream>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <cstring>
#include <string>
#include <cstdlib>
#include <set>
#include <bits/stdc++.h>

#define ENA_LIB_MODBUS  1
#define ENA_LIB_LOG     1

#if ENA_LIB_LOG
#include <plog/Log.h>
#include <plog/Initializers/RollingFileInitializer.h>
#include <plog/Init.h>
#include <plog/Formatters/TxtFormatter.h>
#include <plog/Appenders/ColorConsoleAppender.h>
#endif

#if ENA_LIB_MODBUS
#include <modbus/modbus.h>
#endif

#include "tc_debug.h"
#include "tc_modbus.h"
#include "tc_format.h"
#include "tc_info.h"

#define RETURN_ERROR -1

enum 
{
	FSM_START_R        	= 0,
	FSM_SENSOR_R	  	= 1,
	FSM_BUTTON_R		= 2,
	FSM_BUTTON_CHANGE_R	= 3,
	FSM_NEW_MSG_R		= 4,
	FSM_PING_STATE_R	= 5,
	FSM_PARAMETER_R		= 6,
	FSM_STATE_R			= 7,
	FSM_END_R			= 8,
} fsmRead_e;

enum
{
	FSM_START_W 		= 0,
	FSM_SINGLE_LED_W	= 1,
	FSM_LEDS_W			= 2,
	FSM_PING_RESP_W		= 3,
	FSM_UPDATE_FREQ_W	= 4,
	FSM_STATE_RESP_W	= 5,
	FSM_POSITION_W		= 6,
	FSM_MQTT_W 			= 7,
	FSM_END_W,
} fsmWrite_e;


#endif /* _MAIN_H_ */
