/**
 * @file tc_modbus.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-09-18
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef TC_MODBUS_H
#define TC_MODBUS_H

#include "main.h"
#include "tc_format.h"
#include "tc_info.h"

#define DEVICE_PORT      			"/dev/ttyUSB1"
#define BAUDRATE      				9600

#define MB_SLAVE_ADDRESS       		0x01

/*	READING HOLDING REGISTERs	*/
/* 16 Sensors - 32(regs)*2(2 bytes) = 64 bytes */
#define MB_SENSOR_REG_START_R		0
#define MB_SENSOR_REG_NUM_R   	    32

/* 16 Buttons - 16(regs)*2(2 bytes) = 32 bytes */
#define MB_BUTTON_REG_START_R 	    32
#define MB_BUTTON_REG_NUM_R		    8

#define MB_BUTTON_STATE_REG_R       40

/* ping or new message arrives */
#define MB_PING_REG_R		        41
#define MB_PING_STATE_REG_START_R   42

/* configuration parameters */
#define MB_CONFIG_REG_START_R	    48
#define MB_CONFIG_REG_NUM_R 	    6

#define MB_STATE_REG_START_R        54
#define MB_STATE_REG_NUM_R	        6

/*	WRITING REGISTERs	*/
/* master writes single register to slave */
#define MB_SINGLE_LED_REG_W	        60

/* master writes holding registers to slave */
#define MB_LED_REG_START_W	        61
#define MB_LED_REG_NUM_W    	    3

#define MB_PING_RESP_REG_START_W	64
#define MB_PING_RESP_REG_NUM_W		35

#define MB_CONFIG_FREQ_REG_START_W	99
#define MB_CONFIG_FREQ_REG_NUM_W	2

#define MB_STATE_RESP_REG_START_W	101
#define MB_STATE_RESP_REG_NUM_W		29

#define MB_POSITION_REG_START_W		130
#define MB_POSITION_REG_NUM_W		4

#define MB_MQTT_REG_START_W			156
#define MB_MQTT_REG_NUM_W			51

#define MB_REG_TIMEOUT_W	        57

class ModBus
{
public:
    ModBus();
    ~ModBus();
    void Close(void);
    void Free(void);
    void SetSlave(int slave_addr);
    void SetDevicePort(std::string devicePort);
    void SetBaudRate(int baudRate);
    
    std::string GetDevicePort();
    int GetBaudRate();
    int CheckConnect(void);
    int Connect(const char *devicePort, int baudRate);
    int Connect(void);
    int ReadRegister(int addr, int nb, uint16_t *dest);
    int WriteSingleRegister(int reg_addr, uint16_t value);
    int WriteMultipleRegisters(int addr, int nb, uint16_t *data);
    
    int ReadSensors(Sensor *sensors);
    int ReadButtons(Button *buttons);
    int ReadButtonStateChange(uint16_t &button);
    int ReadPing(uint16_t &ping);
    int ReadState(uint8_t ping, Value &value);
    int ReadParameters(Config &config);

    int WriteSingleLed(uint8_t led, uint8_t value);
    int WriteLeds(uint16_t *data);
    int WritePingResp(uint16_t *data);
    int WriteConfigUpdate(uint16_t *data);
    int WriteStateResp(uint16_t *data);
    int WritePosition(uint16_t *data);
    int WriteMqtt(uint16_t *data);
    
private:
	modbus_t *ctx;
    std::string devicePort;
    int baudRate;
};

#endif 
