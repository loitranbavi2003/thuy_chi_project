/**
 * @file tc_format.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-09-21
 * 
 * @copyright Copyright (c) 2023
 * 
 */
#ifndef TC_FORMAT_H
#define TC_FORMAT_H

#include "main.h"
#include "tc_info.h"
#include <string>
#include <cstring>
#include <stdint.h>
#include <bits/basic_string.h>
#include <vector>

#define ByteToInt16(b1, b2)   			((b1 << 8) | (b2))

typedef union 
{
	float data_float;
	uint16_t bytes[2];	
} data_format_float_uint16;

typedef union
{
	uint16_t data_uint16;
	uint8_t data_uint8[2];
} data_format_uint8_16;

typedef union
{
    uint32_t data_uint32;
    uint16_t bytes[2];
} data_format_uint16_32;

typedef union
{
    uint64_t data_uint64;
    uint16_t bytes[4];
} data_format_uint16_64;

class Formatter
{
public:
    /* position */
    int Data2Register(uint16_t *data, Position &position);

    /* led blink */
    int Data2Register(uint16_t *data, Led &led);

    /* ping response */
    int Data2Register(uint16_t *data, Value &value);

    /* state Response */
    int StateResponse(uint16_t *data, Config &config);

    /* update frequency */
    int Data2Register(uint16_t *data, uint32_t update_freq);

    /* mqtt config */
    int MqttConfig(uint16_t *data, Config &config);
};

#endif // !TC_FORMAT_H