#ifndef TC_DEBUG_H
#define TC_DEBUG_H

#include "main.h"
/* DEBUG for file bts_get_message */
#define ENABLE_DEBUG_GETMESSAGE_FSM 0

/* DEBUG for file bts_frame_message */
#define ENABLE_DEBUG_FRAME_MESSAGE_ERROR 0
#define ENABLE_DEBUG_FRAME_MESSAGE_STATUS 1

/* DEBUG for file bts_system */
#define ENABLE_DEBUG_SYSTEM_ERROR 1
#define ENABLE_DEBUG_SYSTEM_STATUS 1

/* DEBUG for file bts_system */
#define ENABLE_DEBUG_MODBUS_ERROR 1
#define ENABLE_DEBUG_MODBUS_STATUS 1

/* DEBUG for file message */
#define DEBUG_FILE      1
#define DEBUG_CONSOLE   1

class DebugLog
{
public:
    void Log_Init();
    void DebugError(const char *err);
    void DebugError(std::string err);
    void DebugStatus(std::string stt);
    void DebugLOG(int header, std::vector<std::string> content);
private:
   
};
#endif 
