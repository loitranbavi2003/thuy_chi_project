/**
 * @file tc_info.h
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2023-09-22
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#ifndef TC_INFO_H
#define TC_INFO_H

#include <string>
#include <stdint.h>

#define SENSOR_NUM 16
#define BUTTON_NUM 16

class Position
{
public:
	uint8_t longitude[4] = {0};
	uint8_t latitude[4] = {0};

	Position() {};
	~Position() {};
};

class Value
{
public:
    uint8_t data_len = 60;
	bool state;
	uint8_t phone_num[10] = {0};
	uint8_t data[60] = {0};

	Value() {};
	~Value() {};
};

class Config
{
public:
	uint8_t mqtt_state;
	uint8_t reset;
	uint8_t pin_percent;
	uint8_t SIM;
    uint16_t port = 1883;
	uint32_t internet = 0;
	uint32_t mqtt_disc = 0;
	uint32_t update_freq = 30000;
	uint32_t mqtt_conn = 0;
	uint32_t msg_succ = 0;
	float sensor = 0.0;
	float paramA = 0.0;
	float paramB = 0.0;
	float paramC = 0.0;
	uint8_t MAC[6] = {0};
	uint8_t phone_num[10] = {1};
    char username[20] = {'a'};
    char password[20] = {'a'};
	char location[20] = {'a'};
    char host[20] = {'a'};
    char main_sub_topic[20] = {'a'};
    char main_pub_topic[20] = {'a'};

	Config() {};
	~Config() {};
};

class Led
{
public:
	uint8_t led = 13;
	uint8_t value = 255;
	uint16_t on_time = 2000;
	uint16_t off_time = 1000;

	Led() {};
	~Led() {};
};

class Sensor
{
public:
	float value;

	Sensor() {};
	~Sensor() {};
};

class Button
{
public:
	uint8_t state;
	bool pressed;

	Button() {};
	~Button() {};
};

#endif // !TC_INFO_H
