#include <iostream>
#include <string>
#include <vector>
#include <stdint.h>
#include <bitset>
#define ByteToInt16(a, b)   			((a<<8)|(b))
typedef union
{
	uint16_t data_uint16;
	uint8_t data_uint8[2];
} data_format_uint8_16;

int main() {
    std::string macAddress = "00:1A:2B:3C:4D:5E"; // Replace this with your MAC address string

    std::vector<int> macArray; // Create a vector to store the integers
    std::vector<uint16_t> result;
    size_t pos = 0;
    std::string delimiter = ":";
    
    while ((pos = macAddress.find(delimiter)) != std::string::npos) {
        std::string token = macAddress.substr(0, pos); // Extract a component
        int intValue = std::stoi(token, nullptr, 16); // Convert to integer (base 16)
        macArray.push_back(intValue); // Add to the array
        macAddress.erase(0, pos + delimiter.length()); // Move to the next component
    }

    int intValue = std::stoi(macAddress, nullptr, 16);
    macArray.push_back(intValue);

    for(int i=0; i<macArray.size(); i++)
    {
        result.push_back(ByteToInt16(macArray[i], macArray[++i]));
    }

    // Print the array of integers
    std::cout << "MAC Address as Array of Integers: ";
    data_format_uint8_16 tmp816;
    for (uint16_t value : result) {
        tmp816.data_uint16 = value;
        std::cout << static_cast<int>(tmp816.data_uint8[1]) << std::endl;
        std::cout << static_cast<int>(tmp816.data_uint8[0]) << std::endl; 
    }
    std::cout << std::endl;

    return 0;
}
