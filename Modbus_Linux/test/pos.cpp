#include <iostream>
#include <cmath>

using namespace std;

// Function to convert DMS to decimal degrees
double dmsToDecimal(int degrees, int minutes, double seconds, char direction) {
    double decimalDegrees = degrees + minutes / 60.0 + seconds / 3600.0;
    
    if (direction == 'S' || direction == 's' || direction == 'W' || direction == 'w') {
        decimalDegrees = -decimalDegrees;
    }
    
    return decimalDegrees;
}

int main() {
    int degrees_lat, minutes_lat, degrees_lon, minutes_lon;
    double seconds_lat, seconds_lon;
    char direction_lat, direction_lon;
    
    // Input latitude in DMS format
    cout << "Enter latitude in DMS format (e.g., 37 30 15 N): ";
    cin >> degrees_lat >> minutes_lat >> seconds_lat >> direction_lat;
    
    // Input longitude in DMS format
    cout << "Enter longitude in DMS format (e.g., 122 5 30 W): ";
    cin >> degrees_lon >> minutes_lon >> seconds_lon >> direction_lon;
    
    // Convert DMS to decimal degrees
    double latitude = dmsToDecimal(degrees_lat, minutes_lat, seconds_lat, direction_lat);
    double longitude = dmsToDecimal(degrees_lon, minutes_lon, seconds_lon, direction_lon);
    
    // Output the result
    cout << "Latitude in decimal degrees: " << latitude << endl;
    cout << "Longitude in decimal degrees: " << longitude << endl;
    
    return 0;
}
