#include <iostream>
#include <stdint.h>

using namespace std;

int main()
{
    uint8_t std[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
    for (int i=0; i < 10; i++)
    {
        cout << static_cast<int>(std[i]) << std::endl;
    }
}