/*
    X = 0x1234567
    Index:           0    1    2    3
    Big Endian:     01 | 23 | 45 | 67
    Little Endian:  67 | 45 | 23 | 01  
*/

#include <bits/stdc++.h>

int main()
{
	unsigned int i = 1;
	char *c = (char*)&i;
	if (*c)
		std::cout <<"Little endian" << std::endl;
	else
		std::cout <<"Big endian" << std::endl;
	return 0;
}
